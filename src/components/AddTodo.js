import React, { useState } from "react";
import { connect } from "react-redux";
import { addTodo } from "../redux/actions";

const AddTodo = (props) => {
  const [title, setTitle] = useState("");

  const handleform = (e) => {
    setTitle(e.target.value);
  };

  const handlesubmite = (e) => {
    e.preventDefault();
    props.dispatch(addTodo(title));
    setTitle("");
  };
  return (
    <div>
      <form onSubmit={handlesubmite}>
        <input type="text" value={title} onChange={handleform} />
        <button type="submit">ADD</button>
      </form>
    </div>
  );
};

export default connect()(AddTodo);
