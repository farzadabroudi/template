import React from "react";

const Todo = (props) => {
  let { text, onClick, completed } = props;
  return (
    <li
      onClick={onClick}
      style={{ textDecoration: completed ? "line-through" : "none" }}
    >
      {text}
    </li>
  );
};

export default Todo;
