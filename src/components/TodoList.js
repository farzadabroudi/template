import React from "react";
import Todo from "./Todo";
import { connect } from "react-redux";
import { toggleTodo } from "../redux/actions";

const Todolist = (props) => {
  let { todos, dispatch } = props;

  return (
    <ul>
      {todos.map((todo) => (
        <Todo
          key={todo.id}
          {...todo}
          onClick={() => dispatch(toggleTodo(todo.id))}
        />
      ))}
    </ul>
  );
};

const mapStateToprops = (state) => {
  return {
    todos: state.todos,
  };
};

export default connect(mapStateToprops)(Todolist);
